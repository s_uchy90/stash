package com.pawel.stash;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.pawel.stash.adapter.StashAdapter;
import com.pawel.stash.database.DatabaseSourceManager;
import com.pawel.stash.item.Item;

import java.sql.SQLException;
import java.util.ArrayList;


public class ActivityRead extends Activity {

    private DatabaseSourceManager databaseSourceManager;
    private ArrayList<Item> items = new ArrayList<>();
    private StashAdapter adapter;
    private ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);
    }

    protected void onResume() {
        try {
            databaseSourceManager = new DatabaseSourceManager(this);
            databaseSourceManager.open();
            items.addAll(databaseSourceManager.getItems());
            adapter = new StashAdapter(this,R.layout.list_view,items);
            listView = (ListView) findViewById(R.id.list_view);
            listView.setAdapter(adapter);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    protected void onPause() {
        databaseSourceManager.close();
        super.onPause();
    }

}
