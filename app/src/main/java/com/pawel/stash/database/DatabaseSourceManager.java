package com.pawel.stash.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.pawel.stash.item.Item;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by pawel on 08.06.15.
 */
public class DatabaseSourceManager {

    private SQLiteDatabase database;
    private static SQLiteOpenHelper dbHelper;
    private Cursor cursor;

    public DatabaseSourceManager(Context context) {
        dbHelper = DatabaseHelper.getInstance(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();

    }

    public void close() {
        dbHelper.close();
    }

    public boolean isEmpty(String table) {
        boolean empty = true;
        try {
            Cursor cur = database.rawQuery("SELECT COUNT(*) FROM " + table,
                    null);
            if (cur != null && cur.moveToFirst()) {
                empty = (cur.getInt(0) == 0);
            }
            cur.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return empty;
    }

    public void deleteItem(String tableName, String tableId, Integer id)
    // z ksiazki
    {
        database.delete(tableName, tableId + " =?",
                new String[]{id.toString()});
        String countQuery = "SELECT * FROM " + tableName;
        cursor = database.rawQuery(countQuery, null);
    }

    public void deleteAllItem(String tableName) {
        database.delete(tableName, null, null);
    }

    public int getSize(String tableName) {
        int size;
        String countQuery = "SELECT * FROM " + tableName;
        cursor = database.rawQuery(countQuery, null);
        size = cursor.getCount();
        cursor.close();
        return size;
    }

    public Item cursorToItem(Cursor cursor) {
        Item item = new Item();

        int indexName = cursor
                .getColumnIndex(DatabaseNameProvider.CELL_NAME);
        int indexAmount = cursor
                .getColumnIndex(DatabaseNameProvider.CELL_AMOUNT);

        item.setName(cursor.getString(indexName));
        item.setAmount(cursor.getInt(indexAmount));

        return item;
    }

    public ContentValues itemToCursor(Item item) {
        ContentValues values = new ContentValues();
        values.put(DatabaseNameProvider.CELL_NAME, item.getName());
        values.put(DatabaseNameProvider.CELL_AMOUNT, item.getAmount());


        return values;
    }

    public void addItem(Item item) {
        ContentValues values = itemToCursor(item);
        database.insert(DatabaseNameProvider.TABLE_NAME, null, values);
    }

    public ArrayList<Item> getItems() {
        ArrayList<Item> location = new ArrayList<>();

        String countQuery = "SELECT * FROM "
                + DatabaseNameProvider.TABLE_NAME;
        cursor = database.rawQuery(countQuery, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Item item = cursorToItem(cursor);
            location.add(item);
            cursor.moveToNext();
        }
        cursor.close();
        return location;
    }
}

