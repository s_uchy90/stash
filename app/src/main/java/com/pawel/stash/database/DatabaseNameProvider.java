package com.pawel.stash.database;

/**
 * Created by pawel on 08.06.15.
 */
public class DatabaseNameProvider {

    public static final String create = "create table ";
    public static final String update = "drop table if exists ";

    public static final String _ID = "id";
    public static final String TABLE_NAME = "Location";
    public static final String CELL_NAME = "Longitude";
    public static final String CELL_AMOUNT = "Latitude";



    private static String locationTable = TABLE_NAME + " (" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + CELL_NAME
            + " TEXT," + CELL_AMOUNT + " TEXT" + ")";

    public static String getCreateTable(){
        return create + locationTable;
    }

    public static String getUpdateTable(){
        return update + locationTable;
    }
}