package com.pawel.stash.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pawel.stash.R;
import com.pawel.stash.item.Item;

import java.util.ArrayList;

/**
 * Created by pawel on 08.06.15.
 */
public class StashAdapter extends ArrayAdapter {

    private ArrayList<Item> items;
    private int layout;
    private Context context;

    public StashAdapter(Context context, int resource, ArrayList<Item> items) {
        super(context, resource);

        this.context = context;
        this.layout = resource;
        this.items = items;

    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layout, parent, false);
            ViewHolder viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.configureValues(items.get(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView labelView;
        private TextView amountView;

        public ViewHolder(View rowView) {
            labelView = (TextView) rowView.findViewById(R.id.label);
            amountView = (TextView) rowView.findViewById(R.id.amount);
        }

        public void configureValues(Item item) {
            labelView.setText(item.getName());
            amountView.setText(String.valueOf(item.getAmount()));
        }
    }
}
