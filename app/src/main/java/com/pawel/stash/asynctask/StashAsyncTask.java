package com.pawel.stash.asynctask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.pawel.stash.item.Item;

import java.util.ArrayList;

/**
 * Created by pawel on 08.06.15.
 */
public class StashAsyncTask extends AsyncTask<Void , Void , Void > {

    private ArrayList<Item> items = new ArrayList<>();
    private Activity wywolujaceActivity;
    private ProgressDialog progressDialog;


    public StashAsyncTask(Activity activity) {
        this.wywolujaceActivity = activity;
    }

    @Override
    protected Void doInBackground(Void... params) {
        return null;
    }

    @Override
    protected void onPreExecute() {
        ProgressDialog dialog = new ProgressDialog(wywolujaceActivity);
        dialog.setTitle("Odczytuje dane");
        dialog.setMessage("Proszę czekać....");
        dialog.setCancelable(true);
    }


    @Override
    protected void onPostExecute(Void result) {

    }

    public interface onDataReaded{
        void onDataReaded(ArrayList<Item> items);
    }

}
