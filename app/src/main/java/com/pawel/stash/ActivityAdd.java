package com.pawel.stash;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pawel.stash.database.DatabaseSourceManager;
import com.pawel.stash.item.Item;

import java.sql.SQLException;


public class ActivityAdd extends Activity {

    private Button button;
    private EditText edit_name, edit_amount;
    private String name;
    private int amount = -1;
    private DatabaseSourceManager databaseSourceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        databaseSourceManager = new DatabaseSourceManager(this);
        initiateButton();
    }

    private void initiateButton() {
        button = (Button) findViewById(R.id.button_add);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_name.length() != 0 && edit_amount.length() != 0) {
                    name = edit_name.getText().toString();
                    amount = Integer.valueOf(edit_amount.getText().toString());
                    addElement(name, amount);
                } else {
                    Toast.makeText(getBaseContext(), R.string.toast_error, Toast.LENGTH_LONG).show();
                }
            }
        });

        edit_name = (EditText) findViewById(R.id.item_name);
        edit_amount = (EditText) findViewById(R.id.item_amount);
    }

    private void addElement(String name, int amount) {
        try {
            databaseSourceManager.open();
            Item item = new Item(name, amount);
            databaseSourceManager.addItem(item);
            Toast.makeText(this,"Wpis został dodany",Toast.LENGTH_LONG).show();
            clear();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void clear(){
        edit_name.setText("");
        edit_amount.setText("");
    }
    protected void onResume() {
        try {
            databaseSourceManager.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    protected void onPause() {
        databaseSourceManager.close();
        super.onPause();
    }
}
